package space.yhurt.tamagotchi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.media.MediaPlayer;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    Dialog dialog;
    MediaPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Window w = getWindow();
        w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_main);

        Button buttonEnergyOne = findViewById(R.id.buttonOne);
        final TextView textEnergyOne = findViewById(R.id.first);
        ProgressBar progressBarOne = findViewById(R.id.progress_one);

        Button buttonEnergyTwo = findViewById(R.id.buttonTwo);
        final TextView textEnergyTwo = findViewById(R.id.second);
        ProgressBar progressBarTwo = findViewById(R.id.progress_two);

        Button buttonEnergyThree = findViewById(R.id.buttonThree);
        final TextView textEnergyThree = findViewById(R.id.third);
        ProgressBar progressBarThree = findViewById(R.id.progress_three);

        Button buttonEnergyFour = findViewById(R.id.buttonFour);
        final TextView textEnergyFour = findViewById(R.id.fourth);
        ProgressBar progressBarFour = findViewById(R.id.progress_four);

        Button buttonMenu = findViewById(R.id.buttonMenu);
        Button buttonGames = findViewById(R.id.buttonGames);



        AlertDialog.Builder a_builder = new AlertDialog.Builder(MainActivity.this);
        a_builder.setMessage("Слоник умер. Начать заново?")
                .setCancelable(false)
                .setPositiveButton("Да", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.cancel();
                        finish();
                        Intent i = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(i);
                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        finish();
                    }
                });

        CountDownTimer my_timer = new CountDownTimer(1999999999, 60000)
            {
                @Override
                public void onTick(long millisUntilFinished)
                {
                    if (Integer.parseInt(textEnergyOne.getText().toString()) > 0)
                    {
                        int temp1 = Integer.parseInt(textEnergyOne.getText().toString());
                        temp1 -= 1;
                        String tempString1 = Integer.toString(temp1);
                        textEnergyOne.setText(tempString1);
                        int tempBar1 = progressBarOne.getProgress();
                        tempBar1 -= 1;
                        progressBarOne.setProgress(tempBar1);
                    }

                    if (Integer.parseInt(textEnergyTwo.getText().toString()) > 0)
                    {
                        int temp2 = Integer.parseInt(textEnergyTwo.getText().toString());
                        temp2 -= 1;
                        String tempString2 = Integer.toString(temp2);
                        textEnergyTwo.setText(tempString2);
                        int tempBar2 = progressBarTwo.getProgress();
                        tempBar2 -= 1;
                        progressBarTwo.setProgress(tempBar2);
                    }

                    if (Integer.parseInt(textEnergyThree.getText().toString()) > 0)
                    {
                        int temp3 = Integer.parseInt(textEnergyThree.getText().toString());
                        temp3 -= 1;
                        String tempString3 = Integer.toString(temp3);
                        textEnergyThree.setText(tempString3);
                        int tempBar3 = progressBarThree.getProgress();
                        tempBar3 -= 1;
                        progressBarThree.setProgress(tempBar3);
                    }

                    if (Integer.parseInt(textEnergyFour.getText().toString()) > 0)
                    {
                        int temp4 = Integer.parseInt(textEnergyFour.getText().toString());
                        temp4 -= 1;
                        String tempString4 = Integer.toString(temp4);
                        textEnergyFour.setText(tempString4);
                        int tempBar4 = progressBarFour.getProgress();
                        tempBar4 -= 1;
                        progressBarFour.setProgress(tempBar4);
                    }

                    if (Integer.parseInt(textEnergyOne.getText().toString()) <= 0 || Integer.parseInt(textEnergyTwo.getText().toString()) <= 0 || Integer.parseInt(textEnergyThree.getText().toString()) <= 0 || Integer.parseInt(textEnergyFour.getText().toString()) <= 0)
                    {
                        onFinish();
                        cancel();
                    }
                }
                @Override
                public void onFinish()
                {
                    AlertDialog alert = a_builder.create();
                    alert.setTitle("Упс...");
                    alert.show();
                }
            };

        my_timer.start();

        player = MediaPlayer.create(this, R.raw.bg);
        player.setLooping(true);
        player.setVolume(100, 100);
        player.start();

        buttonEnergyOne.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_DOWN && Integer.parseInt(textEnergyOne.getText().toString()) < 100)
                {
                    int temp = Integer.parseInt(textEnergyOne.getText().toString());
                    temp += 1;
                    String tempString = Integer.toString(temp);
                    textEnergyOne.setText(tempString);
                    int tempBar = progressBarOne.getProgress();
                    tempBar += 1;
                    progressBarOne.setProgress(tempBar);

                }
                return false;
            }
        });

        buttonEnergyTwo.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_DOWN && Integer.parseInt(textEnergyTwo.getText().toString()) < 100)
                {
                    int temp = Integer.parseInt(textEnergyTwo.getText().toString());
                    temp += 1;
                    String tempString = Integer.toString(temp);
                    textEnergyTwo.setText(tempString);
                    int tempBar = progressBarTwo.getProgress();
                    tempBar += 1;
                    progressBarTwo.setProgress(tempBar);
                }
                return false;
            }
        });

        buttonEnergyThree.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_DOWN && Integer.parseInt(textEnergyThree.getText().toString()) < 100)
                {
                    int temp = Integer.parseInt(textEnergyThree.getText().toString());
                    temp += 1;
                    String tempString = Integer.toString(temp);
                    textEnergyThree.setText(tempString);
                    int tempBar = progressBarThree.getProgress();
                    tempBar += 1;
                    progressBarThree.setProgress(tempBar);
                }
                return false;
            }
        });

        buttonEnergyFour.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_DOWN && Integer.parseInt(textEnergyFour.getText().toString()) < 100)
                {
                    int temp = Integer.parseInt(textEnergyFour.getText().toString());
                    temp += 1;
                    String tempString = Integer.toString(temp);
                    textEnergyFour.setText(tempString);
                    int tempBar = progressBarFour.getProgress();
                    tempBar += 1;
                    progressBarFour.setProgress(tempBar);
                }
                return false;
            }
        });

        buttonMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(MainActivity.this, buttonMenu);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    public boolean onMenuItemClick(MenuItem item)
                    {
                        int id = item.getItemId();
                        TextView headerView = findViewById(R.id.buttonMenu);
                        switch(id) {
                            case R.id.one:
                                if (Integer.parseInt(textEnergyTwo.getText().toString()) + 30 <= 100) {
                                    int temp1 = Integer.parseInt(textEnergyTwo.getText().toString());
                                    temp1 += 30;
                                    String tempString1 = Integer.toString(temp1);
                                    textEnergyTwo.setText(tempString1);
                                    int tempBar1 = progressBarTwo.getProgress();
                                    tempBar1 += 30;
                                    progressBarTwo.setProgress(tempBar1);
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                else
                                {
                                    Toast.makeText(MainActivity.this, "Слоник столько не съест", Toast.LENGTH_SHORT).show();
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                return true;
                            case R.id.two:
                                if (Integer.parseInt(textEnergyTwo.getText().toString()) + 15 <= 100) {
                                    int temp2 = Integer.parseInt(textEnergyTwo.getText().toString());
                                    temp2 += 15;
                                    String tempString2 = Integer.toString(temp2);
                                    textEnergyTwo.setText(tempString2);
                                    int tempBar2 = progressBarTwo.getProgress();
                                    tempBar2 += 15;
                                    progressBarTwo.setProgress(tempBar2);
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                else
                                {
                                    Toast.makeText(MainActivity.this, "Слоник столько не съест", Toast.LENGTH_SHORT).show();
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                return true;
                            case R.id.three:
                                if (Integer.parseInt(textEnergyTwo.getText().toString()) + 60 < 100)
                                {
                                    int temp3 = Integer.parseInt(textEnergyTwo.getText().toString());
                                    temp3 += 60;
                                    String tempString3 = Integer.toString(temp3);
                                    textEnergyTwo.setText(tempString3);
                                    int tempBar3 = progressBarTwo.getProgress();
                                    tempBar3 += 60;
                                    progressBarTwo.setProgress(tempBar3);
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                else
                                {
                                    Toast.makeText(MainActivity.this, "Слоник столько не съест", Toast.LENGTH_SHORT).show();
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                return true;
                            case R.id.four:
                                if (Integer.parseInt(textEnergyTwo.getText().toString()) + 10 < 100)
                                {
                                    int temp4 = Integer.parseInt(textEnergyTwo.getText().toString());
                                    temp4 += 10;
                                    String tempString4 = Integer.toString(temp4);
                                    textEnergyTwo.setText(tempString4);
                                    int tempBar4 = progressBarTwo.getProgress();
                                    tempBar4 += 10;
                                    progressBarTwo.setProgress(tempBar4);
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                else
                                {
                                    Toast.makeText(MainActivity.this, "Слоник столько не съест", Toast.LENGTH_SHORT).show();
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                return true;
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });

        buttonGames.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(MainActivity.this, buttonGames);
                popup.getMenuInflater().inflate(R.menu.games_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    public boolean onMenuItemClick(MenuItem item)
                    {
                        int id = item.getItemId();
                        TextView headerView = findViewById(R.id.buttonGames);
                        switch(id) {
                            case R.id.one:
                                if (Integer.parseInt(textEnergyFour.getText().toString()) < 100)
                                {
                                    String tempString = Integer.toString(100);
                                    textEnergyFour.setText(tempString);
                                    progressBarFour.setProgress(100);
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                else
                                {
                                    Toast.makeText(MainActivity.this, "Слоник наигрался", Toast.LENGTH_SHORT).show();
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                return true;
                            case R.id.two:
                                if (Integer.parseInt(textEnergyFour.getText().toString()) < 100)
                                {
                                    String tempString = Integer.toString(100);
                                    textEnergyFour.setText(tempString);
                                    progressBarFour.setProgress(100);
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                else
                                {
                                    Toast.makeText(MainActivity.this, "Слоник наигрался", Toast.LENGTH_SHORT).show();
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                return true;
                            case R.id.three:
                                if (Integer.parseInt(textEnergyFour.getText().toString()) < 100)
                                {
                                    String tempString = Integer.toString(100);
                                    textEnergyFour.setText(tempString);
                                    progressBarFour.setProgress(100);
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                else
                                {
                                    Toast.makeText(MainActivity.this, "Слоник наигрался", Toast.LENGTH_SHORT).show();
                                    w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                                }
                                return true;

                        }
                        return true;
                    }
                });
                popup.show();
            }
        });
    }
}
